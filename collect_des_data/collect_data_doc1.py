import json
import pandas as pd
import numpy
from pprint import pprint

# On cherche dans un premier temps à collecter les données qui nous seront utiles pour notre analyse
# On a un fichier .json du site de paris saclay dont on fait un dataframe
# On récupère deux fichiers .cvs qu'il va falloir traiter depuis le site data.gouv


data_file='../donnees/vehicules-gnv-en-circulation-en-france.json'


def collect_Data1(data_file):
    with open(data_file, "r") as f:
       jsondata = json.loads(f.read())
       f.close()


    table_col = []
    table = []


    for dataset in jsondata:
        table_col = list(set().union(dataset["fields"].keys(),table_col)) #Choix des colonnes pour le dataframe
    print(table_col)

    for dataset in jsondata:
        row = []
        for col in table_col:
            row.append(dataset["fields"][col] if col in dataset["fields"] else "")
        table.append(row)

    df = pd.DataFrame(table, index=list(range(len(table))), columns=table_col)

    return(df)



