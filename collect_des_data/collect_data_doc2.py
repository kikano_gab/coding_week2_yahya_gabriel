import json
import pandas as pd
import csv
import numpy
from pprint import pprint

data_file='../donnees/immat.csv'




def collect_Data2(data_file):
    data = []
    with open(data_file, newline='') as csvfile:
       reader = csv.reader(csvfile)
       for row in reader:
           data.append(row)

    header = data[0]
    data = data[1:]

    sections = {}
    current_section = ""

    for row in data:
       if row[0] != '':
           current_section = row[0]
           if current_section not in sections:
               sections[current_section] = []
       sections[current_section].append(row)

    #pprint(sections)

    for table in sections:
        mini_df = pd.DataFrame(sections[table], index=list(range(len(sections[table]))), columns=header)
        mini_df.fillna(value=0)
        print (mini_df)

