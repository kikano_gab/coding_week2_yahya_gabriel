import json
import pandas as pd
import numpy as np
import collect_des_data.collect_data_doc1 as coll
import matplotlib.pylab as plt

def plotTrendGas():

   data_file='./donnees/vehicules-gnv-en-circulation-en-france.json'

   #On veut obtenir les données utiles pour ce doc sur les véhicules GNV

   df = coll.collect_Data1(data_file)
   del df ['parc_des_vehicules_gnv'] #On commence par supprimer la colonne inutile


   # Reste maintenant à les afficher dans un graphe le plus pertinent

   d={}
   for k in range (len(df.nombre_de_vehicule)):
      s=0
      for z in range (len(df.date_anneemois)):
          if (df.date_anneemois[k]==df.date_anneemois[z]):
              c=int(df.nombre_de_vehicule[z])
              s+=c
      d.update({df.date_anneemois[k]:s})

   # On obtient un dictionnaire ayant pour clés les "année-mois" et pour valeur la somme des véhicules GNV pour ce mois
   # On veut maintenant afficher un graphe avec en abscisse les "année-mois" et en ordonnées la valeur de la somme
   # Le problème est que notre dictionnaire n'est pas trié pour avoir un bon graphe
   # On aura besoin d'une fonction de tri de liste: on choisit l'algo de tri rapide

   def quicksort1(t):
      if t == []:
          return []
      else:
          pivot = t[0]
          t1 = []
          t2 = []
          for x in t[1:]:
              if int(x[0])<int(pivot[0]):
                  t1.append(x)
              else:
                  t2.append(x)
      return(quicksort1(t1)+[pivot]+quicksort1(t2))

   #On commence par trier par année. On split les string pour avoir accès à l'année car le format est du type 'année-mois'
   L=[]
   for key in d:
      L+=[key.split('-')]
   M=quicksort1(L) #On obtient une liste des "année-mois" triés par année
   N=sorted(M) #On obtient une liste des "année-mois" triés par année puis par mois pour une meme année


   #Reste maintenant à refaire notre dico trié
   dico={}
   for i in range (len(N)):
      dico.update({N[i][0]+'-'+N[i][1]:d[N[i][0]+'-'+N[i][1]]})
   print (dico)
   #On obtient un dictionnaire trié par année et par mois avec la valeur de la somme des vehicules GNV pour chaque mois




   plt.bar(range(len(dico)), list(dico.values()), align='center')
   plt.xticks(range(len(dico)), list(dico.keys()))

   plt.show()


if __name__ == "__main__":
    plotTrendGas()
