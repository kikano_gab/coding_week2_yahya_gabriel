import json
import pandas
import numpy


#L'objectif de cette première itération est de produire tous les dataframes dont on aura besoin pour notre projet
#On récupère deux fichiers .json en provenance du site (dont on va faire un dataframe)
#On récupère un fichier .cvs du site data.gouv qu'il faudra traiter diféremment

data_file1='/Users/gabrielkikano/Desktop/Coding_Week2_Yahya_Gabriel/immatriculation.json'
data_file2='/Users/gabrielkikano/Desktop/Coding_Week2_Yahya_Gabriel/vehicules_gnv.json'


def dataframe(data_file):
    with open(data_file, "r") as write_file:
        decoded_hand = json.loads(write_file.read())
        write_file.close()
    #print("test", decoded_hand)
    df = pandas.DataFrame(decoded_hand)
    # for k in range (len(decoded_hand)):
    #     print (decoded_hand[k])
    print (df)

