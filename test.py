import pandas as pd
from pprint import pprint
import csv


"""
IMMAC_db = "immat.json"#"immatriculations-par-energie-2017-2if5.json"

with open(IMMAC_db, "r") as f:
    jsondata = json.loads(f.read())
    f.close()

table_col = []
table = []

for dataset in jsondata:
    table_col = list(set().union(dataset["fields"].keys(),table_col))

for dataset in jsondata:
    row = []
    for col in table_col:
        row.append(dataset["fields"][col] if col in dataset["fields"] else "")
    table.append(row)

df = pd.DataFrame(table, index=list(range(len(table))), columns=table_col)

print(df)

"""
"""
csvfile = open('immat.csv', 'r')git 

fieldnames = ("ENERGIE,CO2(g/km)","2003","2004","2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017")

df = pd.read_csv("immat.csv")

print(df)


data = []
"""

def cleanRow(row):

    # headers = ENERGIE,CO2(g/km),2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017

    if '-' in row[1]:
        minn,maxx = row[1].split('-')
        row[1] = (int(minn)+int(maxx))/2
    for i in range(2,len(row)):
        if row[i] == '':
            row[i] = 0
        if type(row[i]) is not int:
            row[i] = row[i].replace(',','')
            row[i] = row[i].replace(' ','')
            row[i] = int(row[i])
    return row


data = []

with open('donnees/immat.csv', newline='') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        data.append(row)

header = data[0]
data = data[1:]

sections = {}
current_section = ""

for row in data:
    if row[0] != '':
        current_section = row[0]
        if current_section not in sections:
            sections[current_section] = []
    sections[current_section].append(cleanRow(row))

#pprint(sections)

for table in sections:
    sections[table] = pd.DataFrame(sections[table], index=list(range(len(sections[table]))), columns=header)

print(sections)

df = sections["Gazole"]


def agg_co2(df):
    data = {"year":[],"total_co2":[]}
    co2 = df["CO2(g/km)"]
    for year in ("2003","2004","2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017"):
        total = 0
        for i in range(len(df)):
            if type(co2[i]) == float or type(co2[i]) == int:
                total += co2[i] * int(df[year][i])
            elif "251" in co2[i]:
                total += 251 * df[year][i]
        data["year"].append(year)
        data["total_co2"].append(total)
    return pd.DataFrame(data=data)


total_co2 = {}

for table in sections:
    total_co2[table] = agg_co2(sections[table])

pprint(total_co2)
