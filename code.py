import pandas as pd
from pprint import pprint
import csv
import plotly.offline as py
import plotly.graph_objs as go
from choix_des_data_utiles1 import plotTrendGas

"""
**********************************   EMISSIONS POUR CHAQUE ANNEE   **********************************
"""

years = ("2003","2004","2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017")

def cleanRow(row):

    # CLEAN and STANDARDISE row data from csv file
    # headers = ENERGIE,CO2(g/km),2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017
    if '-' in row[1]:
        minn,maxx = row[1].split('-')
        row[1] = (int(minn)+int(maxx))/2
    for i in range(2,len(row)):
        if row[i] == '':
            row[i] = 0
        if type(row[i]) is not int:
            row[i] = row[i].replace(',','')
            row[i] = row[i].replace(' ','')
            row[i] = int(row[i])
    return row


def loadCsvData():

    #load csv data from file
    data = []
    header = []

    with open('donnees/immat.csv', newline='', encoding="utf-8") as csvfile:
        reader = csv.reader(csvfile)
        header = next(reader,None)
        for row in reader:
            data.append(row)

    sections = {}
    current_section = ""

    for row in data:
        if row[0] != '':
            current_section = row[0]
            if current_section not in sections:
                sections[current_section] = []
        sections[current_section].append(cleanRow(row))

    for table in sections:
        sections[table] = pd.DataFrame(sections[table], index=list(range(len(sections[table]))), columns=header)

    return sections # returns a dictionary of Dataframes for number of vehicles per emission rating per year, keyed by type


def agg_co2(sect):

    #aggregate data into dataframes, df = total emission per type per year, dfveh = total number of vehicles per type per year
    types = tuple(sect.keys())
    values = []
    nb_vehic_type = []
    vehicles_per_year = [] #pd.DataFrame(columns=("Type",)+years)
    
    for typ in sect:
        row = []
        row_nb_vh = []
        df = sect[typ]  # tableau du calcul de pollution
        co2 = df["CO2(g/km)"]
        for year in years:
            total = 0
            total_veh = 0
            for i in range(len(df)):
                num_veh = int(df[year][i])
                total_veh += num_veh
                if type(co2[i]) == float or type(co2[i]) == int:
                    total += co2[i] * num_veh
                elif "251" in co2[i]:
                    total += 251 * num_veh
                elif "=50" in co2[i] :
                    total += 50 * num_veh
            row.append(total)
            row_nb_vh.append(total_veh)
        values.append(row)
        vehicles_per_year.append(row_nb_vh)  
    return pd.DataFrame(data=values,index=types,columns=years).transpose(), pd.DataFrame(data=vehicles_per_year,index=types,columns=years).transpose()


def outputPlotEmissions(df):
    #plot the trend in emissions per year per type
    data = []

    for column in df.columns:
        if column != 'Toutes énergies' and 'dont' not in column:
            data.append(go.Bar(
                x=df.index,
                y=df[column],
                name=column
            ))

    layout = go.Layout(
        barmode='stack'
    )

    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='emissions-data.html')


def outputPlotVehTrends(df,dfv):
    #plot the trend in vehicles per year
    conventional_types = ['essence + GPL ou gaz naturel ', 'dont : Essence + Gaz Naturel', 'Superéthanol', 'Essence', 'Gazole', 'Autres énergies & ND']
    electric_types = ['Electricité + Essence', 'Electricité + Gazole', 'Electricité ', 'Gaz (GPL ou Gaz Naturel Véhicule)']
    data = [
        go.Scatter(
            x=dfv.index, # assign x as the dataframe column 'x'
            y=dfv[conventional_types].sum(axis=1),
            name="Conventional vehicles"
        ),
        go.Scatter(
            x=df.index, # assign x as the dataframe column 'x'
            y=dfv[electric_types].sum(axis=1),
            name="Electric vehicles"
        )
    ]
    py.plot(data, filename='vehicle-trends.html')

def outputPlotAverageEmissions(df,dfv):
    #plot the average emission per vehicle (conventional, electric) per year
    df_avg = df.copy()
    del dfv["Toutes énergies"]
    del df_avg["Toutes énergies"]

    for col in df_avg.columns:
        for ind in df_avg.index:
            df_avg[col][ind] = df_avg[col][ind] / dfv[col][ind] if dfv[col][ind] > 0 else 0 #divide total emission by number of vehicles per year and type of vehicle
    df_avg = df_avg.transpose().aggregate(lambda x: sum([i for i in x if i != 0])/len([i for i in x if i != 0] if len([i for i in x if i != 0]) > 0 else 0)) #Transpose frame, and aggregate by averaging types of vehicles while excluding empty counts

    data = [
        go.Scatter(
            x=df_avg.index, # assign x as the dataframe column 'x'
            y=df_avg
        )
    ]
    layout = {
        'shapes': [
            # Line Horizontal
            {
                'type': 'line',
                'x0': "2003",
                'y0': 95,
                'x1': "2017",
                'y1': 95,
                'line': {
                    'color': 'rgb(50, 171, 96)',
                    'width': 4,
                    'dash': 'dashdot',
                },
            }
        ]
    }
    fig = {
        'data': data,
        'layout': layout,
    }
    py.plot(fig, filename='avg-emissions.html')


def main():
    
    sections = loadCsvData() #load csv data from file
    df, dfveh = agg_co2(sections) #aggregate data into dataframes, df = total emission per type per year, dfveh = total number of vehicles per type per year
    outputPlotEmissions(df) #plot the trend in emissions per year per type
    outputPlotVehTrends(df,dfveh) #plot the trend in vehicles (conventional, electric) per year
    outputPlotAverageEmissions(df,dfveh) #plot the average emission per vehicle per year
    plotTrendGas()

if __name__ == "__main__":
    main()
